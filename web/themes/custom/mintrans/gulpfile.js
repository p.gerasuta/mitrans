const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

gulp.task('scss', function() {
  return gulp.src('scss/*.scss') // Путь к вашим SCSS файлам
    .pipe(sass())
    .pipe(gulp.dest('css')); // Путь для сохранения скомпилированных CSS файлов
});

gulp.task('watch', function() {
  gulp.watch('scss/*.scss', gulp.series('scss')); // Следить за изменениями в SCSS файлах и автоматически компилировать их при изменениях
});